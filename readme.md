
# Herbarium Api


Herbarium  is an API project allowing the referencing of plants, flowers or trees.
The user will be able to find essential information on the elements present in the database.
-------

## Installation

### Global installation :
`` composer install ``

### Database

edit .env or create local.env 

* `` php bin/console doctrine:database:create ``
* `` php bin/console make:migration ``
* `` php bin/console doctrine:migration:migrate ``

-------
JWT key generator:
(require: openssl)
`` php bin/console lexik:jwt:generate-keypair ``

- [JWT Authentification Documentation ](https://api-platform.com/docs/core/jwt/#improving-tests-suite-speed)

https://api-platform.com/docs/core/jwt/#improving-tests-suite-speed


------
### Create user in DB 

username: 
    useformat@email.ok
role: 
  [] (empty array)

password :
* use hash-password security 
``  php bin/console security:hash-password `` 

## Author

- [@David060881](https://gitlab.com/David060881)
- [WebSite](https://ducrocqdavid.fr)
 