<?php 

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp() : void
    {
        parent::setUp();
        $this->user = new User;
        
    }

    public function testGetEmail():void
    {
        $value = 'test@mail.fr';
        $response = $this->user->setEmail($value);
        $getEmail = $this->user->getEmail();

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $getEmail);
        

    }

    public function testGetRole():void
    {
        $value = ['ROLE_ADMIN'];
        $response = $this->user->setRoles($value);
        self::assertInstanceOf(User::class, $response);
        self::assertContains('ROLE_USER', $this->user->getRoles());
        self::assertContains('ROLE_ADMIN', $this->user->getRoles());
        
    }
    public function testGetPassword():void
    {
        $value = '0000';
        $response = $this->user->setPassword($value);
        self::assertInstanceOf(User::class, $response);
        self::assertEquals( $value, $this->user->getPassword());
        
    }

    
}
